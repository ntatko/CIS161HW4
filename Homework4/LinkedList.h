#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED
class LinkedList {
	private:
		struct ListNode {
			int value;
			ListNode *next;
			ListNode(int val, ListNode *newNode);
		};

		ListNode *head;
	public:
		LinkedList();
		~LinkedList();
		void add(int value);
		void printList();
		void del(int value);
};

#endif // LINKEDLIST_H_INCLUDED
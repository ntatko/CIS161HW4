#include <string>
#include <iostream>
#include <fstream>
#include <time.h>
#include "LinkedList.h"
#include <vector>
#include <sstream>
#include <Windows.h>

using namespace std;

time_t bubbleSort(int sz, vector <int> A);
time_t selectionSort(int sz, vector <int> A);
time_t insertionSort(int sz, vector <int> A);


int main() {
	// initialize variables
	time_t times[3][3];
	vector <int> fromFile;

	string fileNames[3];
	fileNames[0] = "random.txt";
	fileNames[1] = "reversed.txt";
	fileNames[2] = "nearly.txt";

	stringstream streamer;
	
	for (int i = 0; i < 3; i++) {
		// read data into linked list,ext
		// do each action 3 times, to make 
		// 3 linked lists to be sorted
		ifstream nano;
		try {
			nano.open(fileNames[i].c_str());
			if (!nano.is_open()) {
				throw string("You idiot. The file didn't open.");
			}
		}
		catch ( string caught) {
			std::cout << caught << endl;
		}

		int nummer;
		string input;
		getline(nano, input);

		while (!nano.eof()) {
			streamer.clear();
			streamer.str("");
			streamer.str(input);
			streamer >> nummer;
			
			fromFile.push_back(nummer);
			getline(nano, input);
		}
		

		/*BUBBLE SORT*/
		std::cout << "doing the first sort..." << endl;
		times[i][0] = bubbleSort(fromFile.size(), fromFile);
		std::cout << "ending the first sort...." << endl;
		/*SELECTION SORT*/
		std::cout << "doing the second sort..." << endl;
		times[i][1] = selectionSort(fromFile.size(), fromFile);
		std::wcout << "ending the second sort...." << endl;
		/*INSERTION SORT*/

		times[i][2] = insertionSort(fromFile.size(), fromFile);
	}

	string print[3] = { "Random\t\t", "Reversed\t", "Nearly\t\t" };

	std::cout << "\t\tBubble\tSelection\tInsertion" << endl;

	for (int i = 0; i < 3; i++) {
		std::cout << print[i];
		for (int j = 0; j < 3; j++) {
			std::cout << times[i][j] << "\t";
		}
		std::cout << endl;
	}

	system("pause");
}

time_t bubbleSort(int sz, vector <int> A) {
	// start measuring time
	time_t start;
	time(&start);

	// declare variables
	int i = sz - 1;

	// Sort algorithm
	while (i > 0) {
		for (int j = 0; j < i - 1; j++) {
			if (A[j] < A[j + 1]) {
				int holder = A[j];
				A[j] = A[j + 1];
				A[j + 1] = holder;
			}
		}
		i = i - 1;
	}

	// cease measuring time
	time_t end;
	time(&end);

	// return elapsed time
	return end - start;
}

time_t selectionSort(int sz, vector <int> A) {
	// start measuring time
	time_t start;
	time(&start);

	// declare variables
	int remaining = sz-1;
	int smallest;
	int index;

	// Sort algorithm
	while (remaining > 0) {
		smallest = A[0];
		index = 0;
		for (int i = 0; i < remaining; i++) {
			if (smallest > A[i]) {
				smallest = A[i];
				index = i;
			}
		}
		int holder = A[remaining];
		A[remaining] = A[index];
		A[index] = holder;
		remaining = remaining - 1;
	}

	// cease measuring time
	time_t end;
	time(&end);

	// return elapsed time
	return end - start;
}

time_t insertionSort(int sz, vector <int> A) {
	// start meaasuring time
	time_t start;
	time(&start);

	// declare variables
	int i = sz - 2;

	// Sort algorithm
	while (i > 1) {
		int j = i;
		while (j > 0 && A[i] < A[i + 1]) {
			int holder = A[i];
			A[i] = A[i + 1];
			A[i + 1] = holder;
			j = j - 1;
		}
		i = i - 1;
	}

	// cease measuring time
	time_t end;
	time(&end);

	// return elapsed time
	return end - start;
}
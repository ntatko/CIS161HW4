#include "LinkedList.h"
#include <iostream>
using namespace std;

LinkedList::LinkedList() {
	head = NULL;
}

LinkedList::~LinkedList() {}

void LinkedList::add(int value) {

	ListNode *newNode = new ListNode(value, NULL);
	if (head == NULL) {
		head = newNode;
		return;
	}
	ListNode *curr = head;
	while (curr->next != NULL) {
		curr = curr->next;
	}
	curr->next = newNode;
}

void LinkedList::printList() {
	if (head == NULL) {
		cout << "The list is empty\n";
		return;
	}
	ListNode *curr = head;
	while (curr != NULL) {
		cout << curr->value << " ";
		curr = curr->next;
	}
}

void LinkedList::del(int val) {
	if (head == NULL) {
		cout << "Nothing to delete!\n";
		return;
	}
	if (head->value == val) {
		head = head->next;
	}
	ListNode *curr = head;
	while (curr->next != NULL) {
		if (curr->next->value == val) {
			curr->next = curr->next->next;
			break;
		}
		curr = curr->next;
	}
}

LinkedList::LinkedList::ListNode::ListNode(int val, ListNode *newNode) {
	value = val;
	next = newNode;
}